const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { VueLoaderPlugin } = require('vue-loader')
const webpack = require('webpack')
const config = {
  
  mode: 'development',
  devtool: '#eval-source-map' ,
  context: path.resolve(__dirname, '../'),
  entry: {
  dashboard:  './src/app.js',
  
},
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, '../dist'),
    publicPath: '/dist/'
  },
  resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      '@comp': path.resolve(__dirname, '../src/comp/'),
      '@plugins': path.resolve(__dirname, '../src/plugins/'),
      
    }
  },
  module: {
    rules: [
      
        {
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
                loader: "babel-loader"
            },
        },
        {
          test: /\.vue$/,
          loader: 'vue-loader',
          options: {
            loaders: {
            }
            // other vue-loader options go here
          }
        },
        {
          test: /\.(png|jpg|gif|svg)$/,
          loader: 'file-loader',
          options: {
            name: '[name].[ext]?[hash]'
          }
        },
       
        {
          test: /\.(svg|woff|woff2|ttf|eot|otf)([\?]?.*)$/,
          loader: 'file-loader?name=assets/fonts/[name].[ext]',
        },
        {
          test: /\.s(c|a)ss$/,
          use: [
            'vue-style-loader',
            'css-loader',
            {
              loader: 'sass-loader',
              // Requires sass-loader@^8.0.0
              options: {
                implementation: require('sass'),
                sassOptions: {
                  fiber: require('fibers'),
                  indentedSyntax: false // optional
                },
              },
            },
          ],
        },
         {
            test: /\.css$/,
            use: ["style-loader", "css-loader"],
            /*include: [
              path.resolve(__dirname, 'node_modules'),
            ]*/
            
        },
    ]
},
plugins: [
    /*new HtmlWebpackPlugin({
        template: "./index.html"
    }),*/
    new VueLoaderPlugin()
  ],
  devServer: {
    port: 9000,
    //BUG:this fix is insecure.
    //disableHostCheck: true,
    public:"localhost",
    historyApiFallback: true,
    noInfo: true,
    overlay: true,
    proxy: {
      '/api': {
        target: 'http://0.0.0.0:3030/',
        "secure": false,
        "changeOrigin": true,
        //pathRewrite: { '^/api': '' },
        logLevel: 'debug', // this what you want
        proxyTimeout: 1000 * 60 * 10,
        timeout: 1000 * 60 * 10
      },
    },
    headers: {
      "Access-Control-Allow-Origin":"\*"
    }
  },
  
}
module.exports = (env, argv) => {
  const isDevelopment = argv.mode === 'development';
  isDevelopment ? console.log("running in development mode!") : console.log("production mode!")
  
  config.mode= argv.mode;
  config.devtool= isDevelopment
        ? '#eval-source-map' // For development
        : 'source-map';
  return config;
      
}
